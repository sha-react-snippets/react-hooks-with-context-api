import React, { useState } from 'react';
import { ThemeContext, themes } from '../../contexts/themes';

import Container from '../Container';

const App = () => {
  const [theme, setTheme] = useState(themes.light);

  const toggleTheme = () => {
    setTheme(theme === themes.light
      ? themes.dark
      : themes.light);
  };

  return (
    <ThemeContext.Provider value={{ currentTheme: theme, toggleTheme }}>
      <Container />
    </ThemeContext.Provider>
  );
};

export default App;
