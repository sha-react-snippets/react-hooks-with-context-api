import React, { useContext } from 'react';
import { ThemeContext } from '../../contexts/themes';

const Container = () => {
  const {currentTheme, toggleTheme} = useContext(ThemeContext);

  return (
    <button
      style={{
        backgroundColor: currentTheme.button.backgroundColor,
        color: currentTheme.button.color
      }}
      onClick={toggleTheme}
      type="button"
    >
      Toggle Theme
    </button>
  );
}

export default Container;
