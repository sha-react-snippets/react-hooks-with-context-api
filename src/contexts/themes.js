import React from 'react';

export const themes = {
  dark: {
    button: {
      backgroundColor: '#f4f5f7',
      color: '#091E42',
    },
  },

  light: {
    button: {
      backgroundColor: '#007bff',
      color: '#fff',
    },
  },
};

export const ThemeContext = React.createContext({
  currentTheme: themes.light,
  toggleTheme: () => { },
});
